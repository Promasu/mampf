<?php

$gui_data["gebote"] = [
    "Verehrt Euren Koch",
    "Getränke bringt jeder selber mit",
    "Koch, Helfer und Einkäufer müssen nicht abwaschen",
    "Es esse nur wer sich eingetragen",
    "Jeder zahlt das gleiche, egal wieviel er isst",
    "Nachtisch gibt es wenn jeder aufgegessen hat",
    "Wer sich einträgt ohne zu erscheinen, bringt dem Mampf einen Nachtisch",
    "Der Finanzer muss weniger spülen",
    "Du sollst dich bis spätestens Freitag 24:00 anmelden",
    "Nach 2 Jahren Abwesenheit verfällt das Guthaben zugunsten der Mampfkasse"
];

array_walk_recursive($gui_data, function (&$item){$item = htmlentities($item);});
include "template/gebote.template.php";
