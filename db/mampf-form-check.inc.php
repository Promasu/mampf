<?php

$gui_data["messages"] = array();

#first call?
$initial_call = (isset($_POST["form_executed"]) == false);

if(isset($_GET["mampf_id"])){
	#mampf ändern
	$edit_mampf = true;
	$mampf_id = (int)$_GET["mampf_id"];
} else{
	if((isset($_POST["mampf_id"]) == true) && ((int)$_POST["mampf_id"] > 0)){
		$edit_mampf = true;
		$mampf_id = (int)$_POST["mampf_id"];
	} else{
		#neues mampf
		$edit_mampf = false;
		$mampf_id = (int)0;
	}
}
include_once "db/default_mampf_val.inc.php";

$valid_entry = true; //default
$missing_entry = "";

if($initial_call == false){
	#check mandatory fields

	#datum
	if($_POST["date"] == ""){
		$valid_entry = false;
		$missing_entry .= "<br>Datum ";
	}
	if($edit_mampf == false){
		#check if date is already used
		$sql_str = "SELECT count(*) FROM t_mampf WHERE datum = '" . $_POST["date"] . "' AND isCurrent = 1;";
		$mampf_exist = mysqli_fetch_array(CreateQuery($sql_str))[0];
		if($mampf_exist > 0){
			$valid_entry = false;
			$missing_entry .= "<br>Es gibt schon ein Mampf an diesem Tag! ";
		}
	}
	#uhrzeit
	if($_POST["time"] == ""){
		$valid_entry = false;
		$missing_entry .= "<br>Uhrzeit ";
	}
	#ort
	if($_POST["place"] == ""){
		$valid_entry = false;
		$missing_entry .= "<br>Ort ";
	}
	#koch
	if($_POST["cook"] == ""){
		$valid_entry = false;
		$missing_entry .= "<br>Koch ";
	}
}
include_once "db/db_change.inc.php";

if($valid_entry == false){
	array_push($gui_data["messages"], ["type" => "error", "text" =>
		"error invalid entry: " . $missing_entry]);

} elseif($initial_call == false){

	$data = array();
	$data["datum"] = $_POST["date"];
	$data["koch_id"] = (int)$_POST["cook"];
	$data["ort"] = $_POST["place"];
	$data["zeit"] = $_POST["time"];
	$data["max_guest"] = $_POST["max_guest"];
	if($_POST["deadline_date"] < '2018-01-01'){
		$data["deadline"] = $_POST["deadline_date"] . " " . $_POST["deadline_time"];
	}	else{
		$data["deadline"] = date('Y-m-d',strtotime(5-date('w') . ' days')) . " " . '23:59:59';
	}
	$data["comment"] = $_POST["comment"];
	$data["vorspeise"] = $_POST["pre_dish"];
	$data["gericht"] = $_POST["main_dish"];
	$data["beilage"] = $_POST["side_dish"];
	$data["dessert"] = $_POST["dessert"];
	$data["veggi"] = (int)isset($_POST["veggi"]);
	$data["max_helfer"] = $_POST["max_helfer"];

	if($edit_mampf == true){
		#send changes to db
		$data["mampf_id"] = (int)$_POST["mampf_id"];
		$save_success = change_mampf($data);
		if($save_success){
			array_push($gui_data["messages"], ["type" => "success", "text" => "Änderungen wurden gespeichert!"]);
		} else{
			array_push($gui_data["messages"], ["type" => "error", "text" => "Ups da ist ein Fehler passiert!"]);
		}
	} else{
		#send new mampf to db
		$save_success = add_mampf($data);
		if($save_success){
			array_push($gui_data["messages"], ["type" => "success", "text" => "Neues Mampf wurde erstellt!"]);
		} else{
			array_push($gui_data["messages"], ["type" => "error", "text" => "Ups da ist ein Fehler passiert!"]);
		}
	}

}

?>
