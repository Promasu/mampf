<?php
include_once "sql.inc.php";

if(!isset($gui_data)){
    $gui_data = array();
}
$gui_data["user_default"]["is_new"] = $is_new;
$gui_data["user_default"]["mampf_id"] = $mampf_id;

if(!$initial_call && !$create_user){
    #previous val
    $gui_data["user_default"]["guest_id"] = $_POST["guest_id"];
    $gui_data["user_default"]["comment"] = $_POST["comment"];
    $gui_data["user_default"]["isKoch"] = isset($_POST["isKoch"]);
    $gui_data["user_default"]["isHelfer"] = isset($_POST["isHelfer"]);
    $gui_data["user_default"]["isEinkauf"] = isset($_POST["isEinkauf"]);
    $gui_data["user_default"]["isDessert"] = isset($_POST["isDessert"]);
    $gui_data["user_default"]["auslage"] = $_POST["auslage"];
} elseif(!$is_new && !$create_user){
    #daten aus db
    $user = mysqli_fetch_array(get_user_data($mampf_id, $user_id));
    $gui_data["user_default"]["guest_id"] = $user_id;
    $gui_data["user_default"]["comment"] = $user["kommentar"];
    $gui_data["user_default"]["isKoch"] = $user["isKoch"];
    $gui_data["user_default"]["isHelfer"] = $user["isHelfer"];
    $gui_data["user_default"]["isEinkauf"] = $user["isEinkauf"];
    $gui_data["user_default"]["isDessert"] = $user["isDessert"];
    $gui_data["user_default"]["auslage"] = $user["auslage"];
} else{
    #default
    $gui_data["user_default"]["guest_id"] = (int)0;
    $gui_data["user_default"]["comment"] = (string)"";
    $gui_data["user_default"]["isKoch"] = "0";
    $gui_data["user_default"]["isHelfer"] = isset($_GET["is_helper"]);
    $gui_data["user_default"]["isEinkauf"] = isset($_GET["is_buyer"]);
    $gui_data["user_default"]["isDessert"] = isset($_GET["is_dessert"]);
    $gui_data["user_default"]["auslage"] = "0.0";
} 

$user_nbr = 0;
$result = get_all_user();
while($row = mysqli_fetch_array($result)){
    $user_nbr += 1;
    $gui_data["users"][$user_nbr]["name"] = $row["user_name"];
    $gui_data["users"][$user_nbr]["id"] = $row["user_id"];
}

$gui_data["mampf_date"] = mysqli_fetch_array(get_mampfdate($mampf_id))[0];

?>