<?php
    if(isset($gui_data) == false){
        $gui_data = array();
    }

    #qry database for balance
    include_once "sql.inc.php"; 

    $result = get_balance();

    $user_nbr = 0;
    while($row = mysqli_fetch_array($result)){
    	$user_nbr += 1;
    	$gui_data["fin"][$user_nbr]["name"] = $row["user_name"];
    	$gui_data["fin"][$user_nbr]["balance"] = $row["saldo"];
    	$gui_data["fin"][$user_nbr]["einkauf_count"] = $row["einkauf"];
    	$gui_data["fin"][$user_nbr]["helfer_count"] = $row["helfer"];
        $gui_data["fin"][$user_nbr]["mampf_count"] = $row["mampf_count"];
        $gui_data["fin"][$user_nbr]["dessert_due"] = $row["dessert_due"];;
        $gui_data["fin"][$user_nbr]["comment"] = $row["comment"];
        $gui_data["fin"][$user_nbr]["last_mampf"] = $row["last_mampf"];

        #show blanc for 0
        if($gui_data["fin"][$user_nbr]["einkauf_count"] == 0) {
            $gui_data["fin"][$user_nbr]["einkauf_count"] = "";
        }
        if($gui_data["fin"][$user_nbr]["helfer_count"] == 0) {
            $gui_data["fin"][$user_nbr]["helfer_count"] = "";
        }
        if($gui_data["fin"][$user_nbr]["dessert_due"] == 0) {
            $gui_data["fin"][$user_nbr]["dessert_due"] = "";
        }
    }


?>