<?php
    if(isset($gui_data) == false){
        $gui_data = array();
    }

    #show events in timespace
    if(isset($_POST["startdate"])){
        $gui_data["mampf_search"]["startdate"] = $_POST["startdate"];
        $gui_data["mampf_search"]["enddate"] = $_POST["enddate"];
    } else{
        $gui_data["mampf_search"]["startdate"] = date("Y-m-d", mktime(0, 0, 0, date("m")-3, date("d"), date("Y")));
        $gui_data["mampf_search"]["enddate"] = date("Y-m-d", mktime(0, 0, 0, date("m")+1, date("d"), date("Y")));
    }
    #qry database for events
    include_once "sql.inc.php";

    $result = sql_select_main($gui_data["mampf_search"]["startdate"],$gui_data["mampf_search"]["enddate"]);

    $MampfNbr = 0;

    $ExitLoop = false;
    If (($row = mysqli_fetch_array($result)) == false){
        $ExitLoop = true;
    }
    #loop throug qry result and put data in gui_data
    while ($ExitLoop == false) {
        $MampfDat = $row ["datum"];
        $MampfNbr += 1;

        $gui_data["mampflist"][$MampfNbr]["date"] = $row ["datum"];
		    $gui_data["mampflist"][$MampfNbr]["id"] = $row ["mampf_id"];
        $gui_data["mampflist"][$MampfNbr]["time"] = $row ["uhrzeit"];
        $gui_data["mampflist"][$MampfNbr]["created_date"] = Null;
        $gui_data["mampflist"][$MampfNbr]["deadline"] = $row["deadline"];
        $gui_data["mampflist"][$MampfNbr]["max_guest"] = $row["max_guest"];
        $gui_data["mampflist"][$MampfNbr]["location"] = $row["ort"];
        $gui_data["mampflist"][$MampfNbr]["veggi"] = $row["veggi"];
        $gui_data["mampflist"][$MampfNbr]["fee"] = number_format($row["kosten"],2,",",".") . " €";
        if($gui_data["mampflist"][$MampfNbr]["fee"]=="0,00 €"){
            $gui_data["mampflist"][$MampfNbr]["fee"] = "?";
        }
        $gui_data["mampflist"][$MampfNbr]["food"] = [
                "main_dish" => $row["gericht"],
                "side_dish" => $row["beilage"],
                "pre_dish" => $row["vorspeise"],
                "dessert" => $row["dessert"],
                "comment" => $row["m_com"],
        ];

        $UserNbr = 0;
        do {
            $UserNbr += 1;
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["no"] = $UserNbr;
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["id"] = $row["user_id"];
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["name"] = $row["user_name"];
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["comment"] = $row["u_com"];
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["is_cook"] = $row["user_id"]==$row["koch_user_id"];
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["is_buyer"] = $row["isEinkauf"];
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["is_helper"] = $row["isHelfer"];
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["is_dessert"] = $row["isDessert"];
            $gui_data["mampflist"][$MampfNbr]["guests"][$UserNbr]["auslage"] = $row["auslage"];

            If (($row = mysqli_fetch_array($result)) == false){
                $ExitLoop = true;
            }
        } while (($ExitLoop == false)  && ($row ["datum"] === $MampfDat));
    }


?>
