from bs4 import BeautifulSoup
import numpy as np
import re


with open("result2017.xml") as fp:
    soup = BeautifulSoup(fp,  "lxml")

results = []

def clean_string(string):
    a = re.sub('\s+', ' ', string)
    return ' '.join(a.strip().split())

def findMeta(metalist, name):
    try:
        for m in metalist.find_all("meta"):
            if name.lower() in m["type"].lower():
                return clean_string(m["value"])
    except Exception as e:
        print(e)

for mampf in soup.find_all('mampf'):

    date = "todo..."
    time = findMeta(mampf.metadata, "uhrzeit")
    vorspeise = findMeta(mampf.metadata, "vorspeise")
    hauptgericht = findMeta(mampf.metadata, "hauptgericht")
    beilage = findMeta(mampf.metadata, "beilage")
    nachtisch = findMeta(mampf.metadata, "nachtisch")
    kosten = findMeta(mampf.metadata, "kosten")
    einkaufskosten = findMeta(mampf.metadata, "einkaufskosten")
    real_kosten = str( kosten )+ str(einkaufskosten)

    for z in mampf.zusagen.find_all("zusage"):
        person  = clean_string(z['person'])
        bemerkung = clean_string(z['bemerkung'] +z['bemerkung2'])
        results.append([date, time, vorspeise, hauptgericht, beilage,
                       nachtisch, real_kosten, person, bemerkung])

print(results)

npa = np.asarray(results)

np.savetxt("foo.csv", npa, delimiter=",", fmt="%s")

