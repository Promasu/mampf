<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:lxslt="http://xml.apache.org/xslt"
                xmlns:result="http://www.example.com/results"
                extension-element-prefixes="result"
                version="1.0">

    <script language="javascript">
        <![CDATA[
  function callMom()
  {
    alert("hi mom!");
  }
  ]]>
    </script>
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>  <xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

    <xsl:template match="entry">
        Guesser: <xsl:value-of select="player"/>
        <xsl:apply-templates select="guess"/>
    </xsl:template>

    <xsl:template match="guess">
        Guess: <xsl:value-of select="."/>
    </xsl:template>
</xsl:stylesheet>