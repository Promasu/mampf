<div class=" mdl-shadow--4dp mdl-cell mdl-card mdl-cell--12-col mdl-grid mampf">

    <div class="mdl-card__title mdl-color-text--primary">
        <h2 class="mdl-card__title-text">Willkommen beim Mampf!</h2>
    </div>
    <div class="mdl-cell mdl-cell--12-col">
        <p>
            Wenn du hier auf dieser Seite gelandet bist, wurdest du von TTOG oder einem Mampfer eingeladen.
        </p>
        <p>
            Das Qualifiziert dich dazu jetzt und für alle Ewigkeit an Mampfs teilzunehmen.
        </p>

        <b>Was ist Mampf?</b>

        <p> Wir kochen jeden Sonntag Abend für einen Haufen von Personen.
            Lies dir erstmal die Gebote unten durch. Danach trag dich in das nächste Mampf ein.
            Wenn du dein erstes Mal überlebt hast, freuen wir uns über jeden Helfer/Einkäufer, aber auch über jeden
            Mitmampfer.
        </p>

        <b>Wenn du mitmachen willst:</b>

        <ol>
            <li>Suche <a href="/home.php">hier</a> das aktuelle Mampf raus und trage dich ein</li>
            <li>Lies dir die <a href="/gebote.php">Gebote</a> durch</li>
        </ol>

        <b>Wenn du öfter mitmachen willst:</b>
        <ol>
            <li>
                Trage dich in die <a href="https://lists.hadiko.de/wws/info/mampf">Mailingliste</a> ein
            </li>
            <li>
                Lasse Togg in dein Herz
            </li>
        </ol>

        <p>Disclaimer: Die Gebote gelten. Egal ob du sie gelesen hast oder nicht.</p>

    </div>
</div>
