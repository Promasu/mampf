
<?php if (array_key_exists('messages', $gui_data)): ?>

    <?php foreach ($gui_data["messages"] as $message): ?>
        <div class=" mdl-shadow--4dp mdl-cell mdl-card mdl-cell--12-col mdl-grid  <?php
        switch ($message["type"]) {
            case "info":
                echo "mdl-color--grey-300";
                break;
            case "success":
                echo "mdl-color--green-300";
                break;
            case "warning":
                echo "mdl-color--yellow-300";
                break;
            case "error":
                echo "mdl-color--red-300";
                break;
        }
        ?>"><h3 class="mdl-cell">
                <?php
                switch ($message["type"]) {
                    case "info":
                        ?>Information <?php
                        break;
                    case "success":
                        ?>Erfolg <?php
                        break;
                    case "warning":
                        ?>Warnung<?php
                        break;
                    case "error":
                        ?>Fehler<?php
                        break;

                }
                ?></h3>
            <p class="mdl-cell mdl-cell--12-col ">
                <?php echo $message["text"] ?>
            </p>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

