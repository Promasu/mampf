<html>
<?php include "components/head.inc.php" ?>
<body class="mdl-color--grey-100">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <?php include "components/header.inc.php" ?>
    <?php include "components/drawer.inc.php" ?>
    <main class="mdl-layout__content mdl-grid">
        <?php
        $num = 0;
        foreach ($gui_data["gebote"] as $gebot) {
            $num++;
            $text = $gebot;
            include "components/gebot.inc.php";
        }
        ?>
    </main>
</div>
</body>
</html>
